package com.andyken.divideddraggableview;

import com.andyken.divideddraggableview.slice.MainAbilitySlice;
import com.github.andyken.divideddraggableview.DividedDraggableView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
