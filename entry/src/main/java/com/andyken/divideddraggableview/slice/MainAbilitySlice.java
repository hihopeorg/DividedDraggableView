package com.andyken.divideddraggableview.slice;

import com.andyken.divideddraggableview.ResourceTable;
import com.github.andyken.divideddraggableview.DividedDraggableView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.util.ArrayList;
import java.util.Random;

public class MainAbilitySlice extends AbilitySlice {
    private DirectionalLayout rootView;
    private ArrayList<Image> mockViews = new ArrayList<>();
    private Random random = new Random();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initAction();
    }

    private void initAction() {
        initNode();
        initData();
        initView();
    }

    private void initNode() {
        rootView = (DirectionalLayout) findComponentById(ResourceTable.Id_rootView);
    }

    private void initData() {
        for (int i = 0; i < 35; i++) {
            Image view = new Image(MainAbilitySlice.this);
            view.setPixelMap(getThumb(String.valueOf(i)));
            mockViews.add(view);
        }
    }

    private void initView(){
        DividedDraggableView dividedDraggableView = new DividedDraggableView(MainAbilitySlice.this);
        dividedDraggableView.setItemCount(mockViews.size());
        for (Image imageView : mockViews) {
            dividedDraggableView.addChildView(imageView);
        }
        rootView.addComponent(dividedDraggableView);
    }

    private PixelMap getThumb(String s) {
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(150, 150);
        options.pixelFormat = PixelFormat.RGB_565;
        PixelMap bmp = PixelMap.create(options);
        Canvas canvas = new Canvas(new Texture(bmp));
        Paint paint = new Paint();

        paint.setColor(new Color(Color.rgb(random.nextInt(128), random.nextInt(128), random.nextInt(128))));
        paint.setTextSize(30);
        paint.setAntiAlias(true);
        canvas.drawRect(new Rect(0, 0, 150, 150), paint);
        paint.setColor(Color.WHITE);
        paint.setTextAlign(TextAlignment.CENTER);
        canvas.drawText(paint, s, 75, 75);

        return bmp;
    }
}
