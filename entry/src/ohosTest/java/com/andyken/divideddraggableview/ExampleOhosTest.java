package com.andyken.divideddraggableview;

import com.github.andyken.divideddraggableview.DividedDraggableViewCore;
import com.ohos.test.utils.EventHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.After;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ExampleOhosTest {
    @After
    public void tearDown() {
        EventHelper.clearAbilities();
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.andyken.divideddraggableview", actualBundleName);
    }

    @Test
    public void testShowDividedDraggableView() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        assertNotNull("testShowDividedDraggableView failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Field field = dividedDraggableViewCore.getClass().getDeclaredField("usingGroup");
        field.setAccessible(true);
        assertEquals("testShowDividedDraggableView pass", false, field.get(dividedDraggableViewCore));
        assertEquals("testShowDividedDraggableView pass", -1, dividedDraggableViewCore.getDraggedIndex());
    }
}