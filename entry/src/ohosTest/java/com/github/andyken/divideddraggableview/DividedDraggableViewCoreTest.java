package com.github.andyken.divideddraggableview;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.app.Context;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.*;

public class DividedDraggableViewCoreTest {

    @Test
    public void getDraggedChildCount() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        assertEquals("It's right", 0, dividedDraggableViewCore.getDraggedChildCount());
    }

    @Test
    public void getDraggedIndex() throws NoSuchFieldException, IllegalAccessException {
        int draggedIndex = 0;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        Field field = dividedDraggableViewCore.getClass().getDeclaredField("draggedIndex");
        field.setAccessible(true);
        field.set(dividedDraggableViewCore, draggedIndex);
        assertEquals("It's right", draggedIndex, dividedDraggableViewCore.getDraggedIndex());
    }

    @Test
    public void getChildList() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        List<Component> childList = dividedDraggableViewCore.getChildList();
        assertNotNull("It's right", childList);
    }

    @Test
    public void setColCount() throws NoSuchFieldException, IllegalAccessException {
        int colCount = 2;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        dividedDraggableViewCore.setColCount(colCount);
        Field field = dividedDraggableViewCore.getClass().getDeclaredField("colCount");
        field.setAccessible(true);
        assertEquals("It's right", colCount, field.get(dividedDraggableViewCore));
    }

    @Test
    public void setItemHeight() throws NoSuchFieldException, IllegalAccessException {
        int itemHeight = 2;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        dividedDraggableViewCore.setItemHeight(itemHeight);
        Field field = dividedDraggableViewCore.getClass().getDeclaredField("itemHeight");
        field.setAccessible(true);
        assertEquals("It's right", itemHeight, field.get(dividedDraggableViewCore));
    }

    @Test
    public void setyPadding() throws NoSuchFieldException, IllegalAccessException {
        int yPadding = 2;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        dividedDraggableViewCore.setyPadding(yPadding);
        Field field = dividedDraggableViewCore.getClass().getDeclaredField("yPadding");
        field.setAccessible(true);
        assertEquals("It's right", yPadding, field.get(dividedDraggableViewCore));
    }

    @Test
    public void setItemWidth() throws NoSuchFieldException, IllegalAccessException {
        int itemWidth = 2;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        dividedDraggableViewCore.setItemWidth(itemWidth);
        Field field = dividedDraggableViewCore.getClass().getDeclaredField("itemWidth");
        field.setAccessible(true);
        assertEquals("It's right", itemWidth, field.get(dividedDraggableViewCore));
    }

    @Test
    public void setGroupGap() throws NoSuchFieldException, IllegalAccessException {
        int groupGap = 2;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        dividedDraggableViewCore.setGroupGap(groupGap);
        Field field = dividedDraggableViewCore.getClass().getDeclaredField("groupGap");
        field.setAccessible(true);
        assertEquals("It's right", groupGap, field.get(dividedDraggableViewCore));
    }

    @Test
    public void setUsingGroup() throws IllegalAccessException, NoSuchFieldException {
        boolean usingGroup = true;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        dividedDraggableViewCore.setUsingGroup(usingGroup);
        Field field = dividedDraggableViewCore.getClass().getDeclaredField("usingGroup");
        field.setAccessible(true);
        assertEquals("It's right", usingGroup, field.get(dividedDraggableViewCore));
    }

    @Test
    public void setGroupLineCount() throws NoSuchFieldException, IllegalAccessException {
        int groupLineCount = 2;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableViewCore dividedDraggableViewCore = new DividedDraggableViewCore(context);
        dividedDraggableViewCore.setGroupLineCount(groupLineCount);
        Field field = dividedDraggableViewCore.getClass().getDeclaredField("groupLineCount");
        field.setAccessible(true);
        assertEquals("It's right", groupLineCount, field.get(dividedDraggableViewCore));
    }
}