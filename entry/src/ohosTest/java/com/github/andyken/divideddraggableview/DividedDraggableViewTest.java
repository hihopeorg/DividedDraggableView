package com.github.andyken.divideddraggableview;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class DividedDraggableViewTest {

    @Test
    public void setItemCount() throws NoSuchFieldException, IllegalAccessException {
        int itemCount = 5;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableView dividedDraggableView = new DividedDraggableView(context);
        dividedDraggableView.setItemCount(itemCount);
        Field field = dividedDraggableView.getClass().getDeclaredField("itemCount");
        field.setAccessible(true);
        assertEquals("It's right", itemCount, field.get(dividedDraggableView));
    }

    @Test
    public void getPageLineLayout() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableView dividedDraggableView = new DividedDraggableView(context);
        DirectionalLayout pageLineLayout = dividedDraggableView.getPageLineLayout();
        assertNotNull("It's right", pageLineLayout);
    }

    @Test
    public void getTextViewInGapLayout() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableView dividedDraggableView = new DividedDraggableView(context);
        Text textViewInGapLayout = dividedDraggableView.getTextViewInGapLayout();
        assertNotNull("It's right", textViewInGapLayout);
    }

    @Test
    public void getGroupLayout() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableView dividedDraggableView = new DividedDraggableView(context);
        DirectionalLayout groupLayout = dividedDraggableView.getGroupLayout();
        assertNotNull("It's right", groupLayout);
    }

    @Test
    public void dp2px() {
        float dp = 100;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DividedDraggableView dividedDraggableView = new DividedDraggableView(context);
        assertEquals("It's right", 300, dividedDraggableView.dp2px(dp));
    }
}