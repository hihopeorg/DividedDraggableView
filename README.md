# DividedDraggableView

本项目是基于开源项目DividedDraggableView进行ohos化的移植和开发，可以通过项目标签以及github地址（https://github.com/andyken/DividedDraggableView ）追踪到原项目版本

#### 项目介绍

- 项目名称：可拖动的类似GridView控件
- 所属系列：ohos的第三方组件适配移植
- 功能：带分割线，item可拖动。
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/andyken/DividedDraggableView
- 原项目基线版本：无
- 编程语言：Java

#### 演示效果

![Image text](/screenshot/DividedDraggableView.gif)

#### 安装教程

##### 方案一：

1. 下载har包DividedDraggableView.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'DividedDraggableView', ext: 'har')
    ....
}
```

##### 方案二：

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'com.andyken.ohos:divideddraggableview:1.0.0'
 }
```


#### 

#### 使用说明

1. 你可以创建一个DividedDraggableView，调用setItemCount，然后通过addChildView添加你自己的控件：

```java
DividedDraggableView dividedDraggableView = new DividedDraggableView(MainAbilitySlice.this);
dividedDraggableView.setItemCount(mockViews.size());
for (Image imageView : mockViews) {
    dividedDraggableView.addChildView(imageView);
}
rootView.addComponent(dividedDraggableView);
```

2. 你也可以在xml里使用或者通过DividedDraggableView.Builder。

3. 你可以设置自定义属性比如rowHeight,itemWidth,itemHeight,rowPadding等。


#### 版本迭代

- v1.0.0

#### 版权和许可信息

- Apache License, Version 2.0