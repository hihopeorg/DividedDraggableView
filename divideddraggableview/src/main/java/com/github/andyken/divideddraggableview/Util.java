/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.andyken.divideddraggableview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.NoSuchElementException;

public class Util {

    public static int getIntegerValue(AttrSet attrSet, String name, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            int value = attrSet.getAttr(name).get().getIntegerValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static int getDimensionValue(AttrSet attrSet, String name, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            int value = attrSet.getAttr(name).get().getDimensionValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static float getFloatValue(AttrSet attrSet, String name, float defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            float value = attrSet.getAttr(name).get().getFloatValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static boolean getBoolValue(AttrSet attrSet, String name, boolean defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            boolean value = attrSet.getAttr(name).get().getBoolValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static  Color getColorValue(AttrSet attrSet, String name, Color defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            Color value = attrSet.getAttr(name).get().getColorValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static  String getStringValue(AttrSet attrSet, String name, String defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            String value = attrSet.getAttr(name).get().getStringValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }
}
