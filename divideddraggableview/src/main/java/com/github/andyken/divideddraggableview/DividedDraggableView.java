package com.github.andyken.divideddraggableview;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
/**
 * Created by hzfengyuexin on 17/2/10.
 */

public class DividedDraggableView extends ScrollView {

	private DependentLayout rootView;
	private AttrSet attributeSet;
	private int bgColor, gapColor, textInGapColor, groupBgColor;
	private String textInGap;
	private int itemCount;
	private int rowPadding;//the y-axis padding of the item
	private int rowHeight, itemWidth, itemHeight, colCount;
	private boolean usingGroup = false;
	private int groupGap, groupLineCount, groupItemCount;
	private DividedDraggableViewCore dividedDraggableViewCore;

	private static final int DEFAULT_ROW_HEIGHT = 60;
	private static final int DEFAULT_ITEM_WIDTH = 50;
	private static final int DEFAULT_ITEM_HEIGHT = 55;
	private static final int DEFAULT_Y_PADDING = 20;
	private static final boolean DEFAULT_USING_GROUP = false;//using group for default/默认是否使用group
	private static final int DEFAULT_GROUP_ITEM_COUNT = 10;
	private static final int DEFAULT_GROUP_GAP = 35;//default group gap is 35dp/35dp默认group之间的高度
	private static final int DEFAULT_BG_COLOR = Color.getIntColor("#00ffffff");
	private static final int DEFAULT_GAP_COLOR = Color.getIntColor("#f8f8f8");
	private static final int DEFAULT_TEXT_IN_GAP_COLOR = Color.getIntColor("#999999");
	private static final int DEFAULT_GROUP_BG_COLOR = Color.getIntColor("#ffffff");
	private static final String DEFAULT_TEXT_IN_GAP = "page %d";
	private static final int DEFAULT_GROUP_LINE_COUNT = 2;

	public DividedDraggableView(Context context) {
		super(context);
		init();
	}

	public DividedDraggableView(Context context, AttrSet attributeSet) {
		super(context, attributeSet);
		this.attributeSet = attributeSet;
		init();
	}

	public DividedDraggableView(Builder builder) {
		super(builder.context);
		initView();
		this.rowHeight = builder.rowHeight > 0 ? builder.rowHeight : DEFAULT_ROW_HEIGHT;
		this.itemWidth = builder.itemWidth > 0 ? builder.itemWidth : DEFAULT_ITEM_WIDTH;
		this.itemHeight = builder.itemHeight > 0 ? builder.itemHeight : DEFAULT_ITEM_HEIGHT;
		this.rowPadding = builder.yPadding > 0 ? builder.yPadding : DEFAULT_Y_PADDING;
		this.usingGroup = builder.usingGroup;
		this.groupGap = builder.groupGap > 0 ? builder.groupGap : DEFAULT_GROUP_GAP;
		this.groupLineCount = builder.groupLineCount > 0 ? builder.groupLineCount : DEFAULT_GROUP_LINE_COUNT;
		this.groupItemCount = builder.groupItemCount > 0 ? builder.groupItemCount : DEFAULT_GROUP_ITEM_COUNT;
		colCount = groupItemCount / groupLineCount;
		this.bgColor = builder.bgColor > 0 ? builder.bgColor : DEFAULT_BG_COLOR;
		this.gapColor = builder.gapColor > 0 ? builder.gapColor : DEFAULT_GAP_COLOR;
		this.textInGapColor = builder.textInGapColor > 0 ? builder.textInGapColor : DEFAULT_TEXT_IN_GAP_COLOR;
		this.groupBgColor = builder.groupBgColor > 0 ? builder.groupBgColor : DEFAULT_GROUP_BG_COLOR;
		this.textInGap = !TextTool.isNullOrEmpty(builder.textInGap) ? builder.textInGap : DEFAULT_TEXT_IN_GAP;
		initEventListener();
	}

	private void init() {
		initView();
		initAttributes();
		initEventListener();
	}

	private void initAttributes() {
		rowHeight = Util.getDimensionValue(attributeSet, "rowHeight", dp2px(DEFAULT_ROW_HEIGHT));
		itemWidth = Util.getDimensionValue(attributeSet, "itemWidth", dp2px(DEFAULT_ITEM_WIDTH));
		itemHeight = Util.getDimensionValue(attributeSet, "itemHeight", dp2px(DEFAULT_ITEM_HEIGHT));
		rowPadding = Util.getDimensionValue(attributeSet, "yPadding", dp2px(DEFAULT_Y_PADDING));//20dp
		usingGroup = Util.getBoolValue(attributeSet, "usingGroup", DEFAULT_USING_GROUP);
		groupGap = Util.getDimensionValue(attributeSet, "groupGap", dp2px(DEFAULT_GROUP_GAP));//35dp
		groupLineCount = Util.getIntegerValue(attributeSet, "groupLineCount", DEFAULT_GROUP_LINE_COUNT);
		groupItemCount = Util.getIntegerValue(attributeSet, "groupItemCount", DEFAULT_GROUP_ITEM_COUNT);
		colCount = groupItemCount / groupLineCount;
		bgColor = Util.getColorValue(attributeSet, "bgColor", new Color(DEFAULT_BG_COLOR)).getValue();
		gapColor = Util.getColorValue(attributeSet, "gapColor", new Color(DEFAULT_GAP_COLOR)).getValue();
		textInGapColor = Util.getColorValue(attributeSet, "textInGapColor", new Color(DEFAULT_TEXT_IN_GAP_COLOR)).getValue();
		groupBgColor = Util.getColorValue(attributeSet, "groupBgColor", new Color(DEFAULT_GROUP_BG_COLOR)).getValue();
		textInGap = Util.getStringValue(attributeSet, "textInGap", "");
		if (TextTool.isNullOrEmpty(textInGap)) {
			textInGap = DEFAULT_TEXT_IN_GAP;
		}
	}

	private void initView(){
		DirectionalLayout.LayoutConfig scrollViewParam = new DirectionalLayout.LayoutConfig(
				ComponentContainer.LayoutConfig.MATCH_PARENT,
				ComponentContainer.LayoutConfig.MATCH_PARENT);
		setLayoutConfig(scrollViewParam);

		DirectionalLayout linearLayout = new DirectionalLayout(getContext());
		LayoutConfig params = new LayoutConfig(
				LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);
		linearLayout.setLayoutConfig(params);
		linearLayout.setOrientation(DirectionalLayout.VERTICAL);

		rootView = new DependentLayout(getContext());
		linearLayout.addComponent(rootView);
		super.addComponent(linearLayout);

		dividedDraggableViewCore = new DividedDraggableViewCore(getContext());
	}

	private void initEventListener() {
		//解决和scrollview的上下滚动冲突问题
		dividedDraggableViewCore.setActionMoveListener(new DividedDraggableViewCore.ActionListener<TouchEvent>() {
			@Override
			public void onAction(TouchEvent motionEvent) {
				//requestDisallowInterceptTouchEvent(true);
				int scrollDistance = getScrollValue(Component.AXIS_Y);
				//motionEvent.getY 为其在scrollView中的子view的高度 而不是距离屏幕的高度 getRawY为距离屏幕左上角的高度 注意scrollView中的子view比屏幕的高度要大
				int y = Math.round(motionEvent.getPointerScreenPosition(motionEvent.getIndex()).getY());
				int translatedY = y - scrollDistance;
				int threshold = 50;
				// scrollview 向下移动 将上面的内容显示出来
				if (translatedY < threshold) {
					scrollBy(0, -30);
				}
				// scrollview 向上移动 将下面的内容显示出来
				if (translatedY + threshold > getHeight()) {
					// make a scroll down by 30 px
					scrollBy(0, 30);
				}
			}
		});

		dividedDraggableViewCore.setActionUpListener(new DividedDraggableViewCore.ActionListener<TouchEvent>() {
			@Override
			public void onAction(TouchEvent motionEvent) {
			}
		});
	}

	private void initDraggableView(){
		int groupCount = 0;
		int rowCount = 0;
		if (itemCount > 0) {
			groupCount = (int) Math.ceil((double) itemCount / groupItemCount);
			rowCount = (int) Math.ceil((double) itemCount / colCount);
		}
		//每个分组区域高度为 每行高度*行数 + 每行间隔高度*（行数+1) 这里 两行存在三个每行间隔高度
		int groupHeight = rowHeight * DEFAULT_GROUP_LINE_COUNT + rowPadding * (DEFAULT_GROUP_LINE_COUNT + 1);
		int pageLineHeight = groupGap;//“第一页 第二页” 所在行的line的高度
		ArrayList<Integer> topMarginArray = new ArrayList<>();//“第一页 第二页” 所在行距离顶部的margin集合
		//add 分组间隔区域
		if (groupCount > 0) {
			for (int i = 0; i < groupCount; i++) {
				DirectionalLayout linearLayout = getPageLineLayout();
				Text textView = getTextViewInGapLayout();
				textView.setText(String.format(textInGap, i + 1));
				linearLayout.addComponent(textView);
				DependentLayout.LayoutConfig params = new DependentLayout.LayoutConfig(DependentLayout.LayoutConfig.MATCH_PARENT, pageLineHeight);
				//每个间隔分组区域 距离顶部的距离 i为分组间隔区域个数
				//分组间隔区域个数*间隔区域高度 + 分组间隔区域个数*（分组中的行数*行高+（分组中的行数+1)*行间隔）两行存在三个行间隔
				int topMargin = i * groupGap + (i * (DEFAULT_GROUP_LINE_COUNT * rowHeight + (DEFAULT_GROUP_LINE_COUNT + 1) * rowPadding));
				topMarginArray.add(topMargin);
				params.setMargins(0, topMargin, 0, 0);
//				params.addRule(RelativeLayout.BELOW, R.id.categorySort_actionBar);
				linearLayout.setLayoutConfig(params);
				rootView.addComponent(linearLayout);
			}
		}

		//由于可拖动区域是透明的 这里需要绘制其白底
		for (int topMargin : topMarginArray) {
			DirectionalLayout groupLayout = getGroupLayout();
			DependentLayout.LayoutConfig groupLayoutParams = new DependentLayout.LayoutConfig(DependentLayout.LayoutConfig.MATCH_PARENT, groupHeight);
//			pageLineGapParams.addRule(RelativeLayout.BELOW, R.id.categorySort_actionBar);
			groupLayoutParams.setMargins(0, topMargin + groupGap, 0, 0);
			groupLayout.setLayoutConfig(groupLayoutParams);
			rootView.addComponent(groupLayout);
		}
		//add 可拖动区域
		//可拖动区域高度为 间隔分组的高度*分组个数 +该高度下面的一个padding*分组个数 +每行高度*行数 + 每行间隔高度*行数
//		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
//				rowheight * rowCount + groupGap + rowPadding * groupCount + rowCount * rowPadding + groupCount * groupGap);
		DirectionalLayout.LayoutConfig layoutParams = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,
				groupGap * groupCount + rowPadding * groupCount + rowHeight * rowCount + rowPadding * rowCount);
//		layoutParams.addRule(RelativeLayout.BELOW, R.id.categorySort_actionBar);
		dividedDraggableViewCore.setLayoutConfig(layoutParams);
		dividedDraggableViewCore.setItemHeight(rowHeight);
		dividedDraggableViewCore.setItemWidth(itemWidth);
		dividedDraggableViewCore.setColCount(colCount);
		dividedDraggableViewCore.setyPadding(rowPadding);
		dividedDraggableViewCore.setGroupGap(groupGap);
		dividedDraggableViewCore.setUsingGroup(true);
		dividedDraggableViewCore.setGroupLineCount(groupLineCount);
		ShapeElement element = new ShapeElement();
		element.setRgbColor(RgbColor.fromArgbInt(bgColor));
		dividedDraggableViewCore.setBackground(element);
		rootView.addComponent(dividedDraggableViewCore);
	}

	/**
	 * should init first
	 * @param itemCount
	 */
	public void setItemCount(int itemCount){
		this.itemCount = itemCount;
		initDraggableView();
	}

	public void addChildView(Component child) {
		if (dividedDraggableViewCore != null) {
			dividedDraggableViewCore.addComponent(child);
		}
	}

	public DirectionalLayout getPageLineLayout() {
		DirectionalLayout linearLayout = new DirectionalLayout(getContext());
		ShapeElement element = new ShapeElement();
		element.setRgbColor(RgbColor.fromArgbInt(gapColor));
		linearLayout.setBackground(element);
		linearLayout.setAlignment(LayoutAlignment.VERTICAL_CENTER);
		return linearLayout;
	}

	public Text getTextViewInGapLayout() {
		Text textView = new Text(getContext());
		textView.setTextSize(15, Text.TextSizeType.FP);
		textView.setTextColor(new Color(textInGapColor));
		DirectionalLayout.LayoutConfig layoutParams = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
		layoutParams.setMargins(dp2px(15), 0, 0, 0);
		textView.setLayoutConfig(layoutParams);
		return textView;
	}

	public DirectionalLayout getGroupLayout() {
		DirectionalLayout linearLayout = new DirectionalLayout(getContext());
		ShapeElement element = new ShapeElement();
		element.setRgbColor(RgbColor.fromArgbInt(groupBgColor));
		linearLayout.setBackground(element);
		return linearLayout;
	}

	/**
	 * dp转pixel
	 */
	public int dp2px(float dp) {
		return AttrHelper.vp2px(dp, getContext());
	}

	public static class Builder{
		private Context context;
		private int rowHeight;
		private int itemWidth;
		private int itemHeight;
		private int yPadding;
		private boolean usingGroup = DEFAULT_USING_GROUP;
		private int groupGap;
		private int groupLineCount;
		private int groupItemCount;
		private int bgColor;
		private int gapColor;
		private int textInGapColor;
		private int groupBgColor;
		private String textInGap;

		public Builder setRowHeight(Context context) {
			this.context = context;
			return this;
		}

		public Builder setRowHeight(int rowHeight) {
			this.rowHeight = rowHeight;
			return this;
		}

		public Builder setItemWidth(int itemWidth) {
			this.itemWidth = itemWidth;
			return this;
		}

		public Builder setItemHeight(int itemHeight) {
			this.itemHeight = itemHeight;
			return this;
		}

		public Builder setyPadding(int yPadding) {
			this.yPadding = yPadding;
			return this;
		}

		public Builder setUsingGroup(boolean usingGroup) {
			this.usingGroup = usingGroup;
			return this;
		}

		public Builder setGroupGap(int groupGap) {
			this.groupGap = groupGap;
			return this;
		}

		public Builder setGroupLineCount(int groupLineCount) {
			this.groupLineCount = groupLineCount;
			return this;
		}

		public Builder setGroupItemCount(int groupItemCount) {
			this.groupItemCount = groupItemCount;
			return this;
		}

		public Builder setBgColor(int bgColor) {
			this.bgColor = bgColor;
			return this;
		}

		public Builder setGapColor(int gapColor) {
			this.gapColor = gapColor;
			return this;
		}

		public Builder setTextInGapColor(int textInGapColor) {
			this.textInGapColor = textInGapColor;
			return this;
		}

		public Builder setGroupBgColor(int groupBgColor) {
			this.groupBgColor = groupBgColor;
			return this;
		}

		public Builder setTextInGap(String textInGap) {
			this.textInGap = textInGap;
			return this;
		}
	}
}
